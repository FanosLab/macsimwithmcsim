#include <stdio.h>
#include "pin.H"
#include "instlib.H"
#include <map>
#include <fstream>


KNOB<UINT32> KnobTrace(KNOB_MODE_WRITEONCE, "pintool", "t", "0", 
			     "Enable of Disable trace");

KNOB<string> KnobOutputFile1(KNOB_MODE_WRITEONCE, "pintool", "o1", "branch_trace.out", 
			     "specify branch trace file name");

KNOB<string> KnobOutputFile2(KNOB_MODE_WRITEONCE, "pintool", "o2", "memory_trace.out", 
			     "specify memory trace file name");

KNOB<UINT32> KnobNumL1Sets(KNOB_MODE_WRITEONCE, "pintool", "l1s", "128",
                           "Number of L1 sets");

KNOB<UINT32> KnobNumL1Blocks(KNOB_MODE_WRITEONCE, "pintool", "l1b", "4",
                             "Number of blocks per L1 set");


FILE * trace1;
FILE * trace2;
UINT32 num_memrefs=0;
UINT32 num_branchrefs=0;

INSTLIB::ICOUNT icount;
INSTLIB::CONTROL control;
bool doinst;

#define MAX_REFS 1000000 	//specify the maximum number of memory references you want to simulate
#define BRANCH_REFS 500000 	//specify the maximum number of branch references you want to simulate


// memory read record
VOID RecordMemRead(ADDRINT ip, ADDRINT addr)
{
    if(num_memrefs <= MAX_REFS)
    {
	if(KnobTrace.Value())
	    fprintf(trace2,"R %lx\n", addr);
        
        ++num_memrefs;
    
        //Insert Cache Simulation code here
    }
}

// memory write record
VOID RecordMemWrite(ADDRINT ip, ADDRINT addr)
{
    if(num_memrefs <= MAX_REFS)
    {
	if(KnobTrace.Value())
	    fprintf(trace2,"W %lx\n", addr);
        
        ++num_memrefs;
    
        //Insert Cache Simulation code here
    }

}

// branch record
VOID RecordBranch(ADDRINT ip, ADDRINT addr, INT32 taken)
{
    if(num_branchrefs <= BRANCH_REFS)
    {
	if(KnobTrace.Value())
	    fprintf(trace1,"B %lx %d\n", ip,taken);

	//Insert Branch Predictor code here	

    }
}

// Is called for every instruction and instruments reads and writes
VOID Instruction(INS ins, VOID *v)
{  
    
    if(doinst)
    {
	    // Instruments memory accesses
	    // Predicated call used so that access is instrumented iff the access is actually executed
            if (INS_IsMemoryRead(ins))
	    {
	 	INS_InsertPredicatedCall(
			ins, IPOINT_BEFORE, (AFUNPTR)RecordMemRead,
			IARG_INST_PTR,
			IARG_MEMORYREAD_EA,
			IARG_END);
	    }

	    if (INS_IsMemoryWrite(ins))
	    {
	 	INS_InsertPredicatedCall(
			ins, IPOINT_BEFORE, (AFUNPTR)RecordMemWrite,
			IARG_INST_PTR,
			IARG_MEMORYWRITE_EA,
			IARG_END);
	    }

   
	    //Instruments branch instructions
	    //system calls and traps to the OS are not instrumented
	    if (INS_IsBranchOrCall(ins))
	    {
		INS_InsertCall(
			ins, IPOINT_BEFORE, (AFUNPTR) RecordBranch, 
			IARG_INST_PTR, 
			IARG_BRANCH_TARGET_ADDR, 
			IARG_BRANCH_TAKEN, 
			IARG_END);
	    }

    }
}

VOID Fini(INT32 code, VOID *v)
{
    if(KnobTrace.Value())
    {
        fclose(trace1);
        fclose(trace2);
    }

    //Print the results here

}

/* ===================================================================== */
/* Print Help Message                                                    */
/* ===================================================================== */
   
INT32 Usage()
{
    PIN_ERROR( "This Pintool prints a trace of memory addresses and branch instructions\n" 
              + KNOB_BASE::StringKnobSummary() + "\n");
    return -1;
}


/* ===================================================================== */
/* Control                                                               */
/* ===================================================================== */


VOID Handler(INSTLIB::CONTROL_EVENT ev, VOID * v, CONTEXT * ctxt, VOID * ip, THREADID tid)
{
    switch(ev)
    {
      case INSTLIB::CONTROL_START:
        cout << "Starting instrumentation..." << endl;
                doinst = true;
        break;

      case INSTLIB::CONTROL_STOP:
        cout << "Stopping instrumentation..." << endl;
                doinst = false;
        break;

      default:
        ASSERTX(false);
        break;
    }
}




/* ===================================================================== */
/* Main                                                                  */
/* ===================================================================== */

int main(int argc, char *argv[])
{
    //Initialize the Pin tool
    if (PIN_Init(argc, argv)) return Usage();


    if(KnobTrace.Value())
    {
        trace1 = fopen(KnobOutputFile1.Value().c_str(), "w");
        trace2 = fopen(KnobOutputFile2.Value().c_str(), "w");
    }

    // Activate instruction counter
    icount.Activate();

    // Activate alarm, must be done before PIN_StartProgram
    control.CheckKnobs(Handler, 0);

    //initialize doinst to false
    doinst = false;

    //initialize cache dimensions
    //use KnobNumL1Sets.Value() & KnobNumL1Blocks.Value() to set cache dimensions

    //Instrumentation Function - called for every instruction executed by the application binary
    INS_AddInstrumentFunction(Instruction, 0);

    //Finish Function - called when execution is complete.
    PIN_AddFiniFunction(Fini, 0);

    // Start analysis - Never returns
    PIN_StartProgram();
    
    return 0;
}

