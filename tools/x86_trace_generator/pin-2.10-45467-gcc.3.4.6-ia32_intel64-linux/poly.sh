#!/bin/bash
# My first script

sudo ./pin -t ../obj-intel64/trace_generator.so -- bin/2mm 
mv trace_0.raw traces/POLYBENCH/2mm/2mm_0.raw
mv trace.txt traces/POLYBENCH/2mm/2mm.txt

sudo ./pin -t ../obj-intel64/trace_generator.so -- bin/3mm 
mv trace_0.raw traces/POLYBENCH/3mm/3mm_0.raw
mv trace.txt traces/POLYBENCH/3mm/3mm.txt

sudo ./pin -t ../obj-intel64/trace_generator.so -- bin/atax 
mv trace_0.raw traces/POLYBENCH/atax/atax_0.raw
mv trace.txt traces/POLYBENCH/atax/atax.txt

sudo ./pin -t ../obj-intel64/trace_generator.so -- bin/bicg 
mv trace_0.raw traces/POLYBENCH/bicg/bicg_0.raw
mv trace.txt traces/POLYBENCH/bicg/bicg.txt

sudo ./pin -t ../obj-intel64/trace_generator.so -- bin/cholesky 
mv trace_0.raw traces/POLYBENCH/cholesky/cholesky_0.raw
mv trace.txt traces/POLYBENCH/cholesky/cholesky.txt

sudo ./pin -t ../obj-intel64/trace_generator.so -- bin/doitgen 
mv trace_0.raw traces/POLYBENCH/doitgen/doitgen_0.raw
mv trace.txt traces/POLYBENCH/doitgen/doitgen.txt

sudo ./pin -t ../obj-intel64/trace_generator.so -- bin/gemm 
mv trace_0.raw traces/POLYBENCH/gemm/gemm_0.raw
mv trace.txt traces/POLYBENCH/gemm/gemm.txt

sudo ./pin -t ../obj-intel64/trace_generator.so -- bin/gemver 
mv trace_0.raw traces/POLYBENCH/gemver/gemver_0.raw
mv trace.txt traces/POLYBENCH/gemver/gemver.txt
